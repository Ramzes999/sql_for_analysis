SELECT
    p.prod_category,
    t.calendar_year,
    t.calendar_month_number,
    SUM(s.amount_sold) AS total_sales_amount
FROM
    sales s
JOIN
    products p ON s.prod_id = p.prod_id
JOIN
    times t ON s.time_id = t.time_id
GROUP BY
    p.prod_category, t.calendar_year, t.calendar_month_number;

   
SELECT
    c2.country_id ,
    s.prod_id,
    AVG(s.quantity_sold) AS average_sales_quantity
FROM
    sales s
JOIN
    customers c ON s.cust_id = c.cust_id
JOIN
    countries c2 ON c.country_id  = c2.country_id 
GROUP BY
    c2.country_id , s.prod_id;

   
SELECT
    c.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    SUM(s.amount_sold) AS total_sales_amount
FROM
    sales s
JOIN
    customers c ON s.cust_id = c.cust_id
GROUP BY
    c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY
    total_sales_amount DESC
LIMIT 5;


